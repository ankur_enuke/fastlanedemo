fastlane documentation
================
# Installation

Make sure you have the latest version of the Xcode command line tools installed:

```
xcode-select --install
```

Install _fastlane_ using
```
[sudo] gem install fastlane -NV
```
or alternatively using `brew cask install fastlane`

# Available Actions
## Android
### android test
```
fastlane android test
```
Runs all the tests
### android screenshots
```
fastlane android screenshots
```

### android beta
```
fastlane android beta
```
Submit a new Beta Build to Crashlytics Beta
### android betaupload
```
fastlane android betaupload
```

### android deploy
```
fastlane android deploy
```
Deploy a new version to the Google Play
### android storerollout
```
fastlane android storerollout
```

### android buildkeystoreandroid
```
fastlane android buildkeystoreandroid
```

### android changeAndroidPackageName
```
fastlane android changeAndroidPackageName
```


----

This README.md is auto-generated and will be re-generated every time [fastlane](https://fastlane.tools) is run.
More information about fastlane can be found on [fastlane.tools](https://fastlane.tools).
The documentation of fastlane can be found on [docs.fastlane.tools](https://docs.fastlane.tools).
