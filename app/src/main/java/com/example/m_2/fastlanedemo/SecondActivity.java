package com.example.m_2.fastlanedemo;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView mWelcome;
    private Button mBackGo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        initView();
    }

    private void initView() {
        mWelcome = (TextView) findViewById(R.id.welcome);
        mWelcome.setOnClickListener(this);
        mBackGo = (Button) findViewById(R.id.go_back);
        mBackGo.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.welcome:
                // TODO 19/03/01
                break;
            case R.id.go_back:// TODO 19/03/01
                onBackPressed();
                break;
            default:
                break;
        }
    }
}
